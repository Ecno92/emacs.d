(use-package flycheck :ensure t
  :config (global-flycheck-mode))

(provide 'init-flycheck)
