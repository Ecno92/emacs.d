;; init-ui.el --- Initialize ui

;;; Commentary:

;;; Code:
(use-package dracula-theme :ensure t
  )

(setq ns-use-native-fullscreen nil)
(tool-bar-mode -1)
(menu-bar-mode -1)
(if (fboundp 'scroll-bar-mode)
    (scroll-bar-mode -1))

(custom-set-variables
 '(initial-frame-alist (quote ((fullscreen . maximized)))))

;; Highlight uncommitted changes (https://github.com/seagle0128/.emacs.d/blob/01e78a908c89b7390ea6f0ee215eb791f1d330c4/lisp/init-highlight.el)
(use-package diff-hl
  :defines desktop-minor-mode-table
  :commands diff-hl-magit-post-refresh
  :custom-face
  (diff-hl-change ((t (:background "#46D9FF"))))
  (diff-hl-delete ((t (:background "#ff6c6b"))))
  (diff-hl-insert ((t (:background "#98be65"))))
  :bind (:map diff-hl-command-map
              ("SPC" . diff-hl-mark-hunk))
  :hook ((after-init . global-diff-hl-mode)
         (dired-mode . diff-hl-dired-mode))
  :config
  ;; Highlight on-the-fly
  (diff-hl-flydiff-mode 1)

  ;; Set fringe style
  (setq diff-hl-draw-borders nil)
  (setq fringes-outside-margins t)

  (unless (display-graphic-p)
    ;; Fall back to the display margin since the fringe is unavailable in tty
    (diff-hl-margin-mode 1)
    ;; Avoid restoring `diff-hl-margin-mode'
    (with-eval-after-load 'desktop
      (add-to-list 'desktop-minor-mode-table
                   '(diff-hl-margin-mode nil))))

  ;; Integration with magit
  (with-eval-after-load 'magit
(add-hook 'magit-post-refresh-hook #'diff-hl-magit-post-refresh)))

(defun ui-after-init ()
  (if (display-graphic-p)
      (load-theme 'dracula 'no-confirm)))

(add-hook 'after-init-hook 'ui-after-init)

(setq visible-bell t)

(provide 'init-ui)
;;; init-ui.el ends here
