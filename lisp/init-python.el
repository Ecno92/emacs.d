(use-package python :ensure t
  :hook
  (python-mode . eglot-ensure)
  :config
  (add-hook 'before-save-hook 'eglot-format)
  )

(setq python-shell-interpreter "python3")
(setq flycheck-python-flake8-executable "flake8")

(use-package py-isort :ensure t
  :config
  (add-hook 'before-save-hook 'py-isort-before-save))

(defun my-shell-mode-hook ()
  (add-hook 'comint-output-filter-functions 'python-pdbtrack-comint-output-filter-function t))
(add-hook 'shell-mode-hook 'my-shell-mode-hook)

(provide 'init-python)
