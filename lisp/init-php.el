;; init-php.el --- Initialize php

;;; Commentary:

;;; Code:

(use-package php-mode
  :ensure t
  :mode ("\\.php\\'" . php-mode)
)

(provide 'init-php)
;;; init-php.el ends here
