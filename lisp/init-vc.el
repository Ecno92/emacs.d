;;; init-vc.el --- Initiliaze version control support

;;; Commentary:

;;; Code:
(use-package magit
  :ensure t
  :bind (("C-x g" . magit-status)
         ("C-x M-g" . magit-dispatch-popup)
         ("C-c M-g" . magit-file-popup)))

(use-package forge
  :ensure t
  :after magit)

(use-package diff-hl :ensure t)
(use-package gitignore-mode :ensure t)
(use-package gitconfig-mode :ensure t)
(use-package browse-at-remote :ensure t)

(provide 'init-vc)
;;; init-vc.el ends here
