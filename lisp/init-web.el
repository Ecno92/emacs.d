;; init-web.el --- Initialize Web and JS related stuff

;;; Commentary:

;;; Code:
(use-package web-mode :ensure t)

(use-package prettier-js :ensure t)
(use-package js2-mode  :ensure t)
(use-package rjsx-mode :ensure t)
(use-package add-node-modules-path :ensure t)

(add-to-list 'auto-mode-alist '("\\.js\\'" . js2-mode))
(add-to-list 'auto-mode-alist '("\\.jsx\\'" . rjsx-mode))
(add-to-list 'auto-mode-alist '("\\.html\\'" . web-mode))

;; Turn off js2 mode errors & warnings (we lean on eslint/standard)
(setq js2-mode-show-parse-errors nil)
(setq js2-mode-show-strict-warnings nil)
(setq js-indent-level 2)
(setq web-mode-markup-indent-offset 2)
(setq web-mode-css-indent-offset 2)
(setq web-mode-code-indent-offset 2)
(setq web-mode-script-padding 0)
(setq web-mode-style-padding 0)

(add-hook 'js2-mode-hook 'devenv-hook)
(add-hook 'rjsx-mode-hook 'devenv-hook)
(add-hook 'web-mode-hook 'devenv-hook)

(defun devenv-hook ()
  (add-node-modules-path)
  (prettier-js-mode))

(define-derived-mode genehack-vue-mode web-mode "ghVue"
  "A major mode derived from web-mode, for editing .vue files with LSP support.")
(add-to-list 'auto-mode-alist '("\\.vue\\'" . genehack-vue-mode))
(add-hook 'genehack-vue-mode-hook #'eglot-ensure)
(add-to-list 'eglot-server-programs '(genehack-vue-mode "vls"))

(provide 'init-web)
;;; init-web.el ends here
