;; ag is needed for C-p ss
(use-package ag :ensure t)
(use-package projectile :ensure t
  :init
  (projectile-global-mode)
  (define-key projectile-mode-map (kbd "C-c p") 'projectile-command-map))
(use-package counsel-projectile :ensure t)

(provide 'init-projectile)
