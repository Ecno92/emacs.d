;; init-eglot.el --- Initialize eglot code auto completion

;;; Code:
(use-package eldoc-box
  :ensure t
  )

(use-package eglot :ensure t
  :config
  (add-hook 'eglot--managed-mode-hook #'eldoc-box-hover-mode t)
  )

(provide 'init-eglot)
;;; init-eglot.el ends here
