(setq dired-auto-revert-buffer t)
(use-package diredfl :ensure t :config (diredfl-global-mode))
(setq dired-dwim-target t)

(provide 'init-dired)
