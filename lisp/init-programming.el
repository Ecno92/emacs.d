(use-package dumb-jump :ensure t :init (dumb-jump-mode))
(set-variable 'flycheck-highlighting-mode 'lines)

(use-package rainbow-delimiters
  :ensure t
  :config
  (add-hook 'prog-mode-hook #'rainbow-delimiters-mode))

(add-hook 'prog-mode-hook 'electric-pair-mode)
;; Disable electric-pair in minibuffer
(defun my/inhibit-electric-pair-mode (char) (minibufferp))
(setq electric-pair-inhibit-predicate #'my/inhibit-electric-pair-mode)

(setq make-backup-files nil)
(setq auto-save-default nil)
(setq create-lockfiles nil)

(provide 'init-programming)
