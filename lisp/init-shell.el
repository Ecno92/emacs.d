(require 'comint)
(setq comint-password-prompt-regexp
      (concat
       "\\("
       "Password for 'http.*':"
       "\\|"
       "\\w+ password:"
       "\\|"
       comint-password-prompt-regexp
       "\\)"
       ))

(use-package exec-path-from-shell :ensure t
  :init
(when (memq window-system '(mac ns x))
  (exec-path-from-shell-initialize)))


(use-package vterm
  :ensure t)

(defun clean-view-vterm ()
  (setq show-trailing-whitespace nil)
  (nlinum-mode -1)
  (beacon-mode -1)
)

(add-hook
 'vterm-mode-hook
 'clean-view-vterm)

(provide 'init-shell)
