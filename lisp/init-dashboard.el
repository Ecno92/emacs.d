;; init-dashboard.el --- Initialize startup dashboard

;;; Commentary:

;;; Code:
(use-package dashboard
  :ensure t
  :config
  (dashboard-setup-startup-hook)
  (setq dashboard-items '(
                          (recents  . 10)
                          (bookmarks . 5)
                          (projects . 5)
                          ))
   )


(provide 'init-dashboard)
;;; init-dashboard.el ends here
