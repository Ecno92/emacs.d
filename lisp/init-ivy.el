(use-package smex  :ensure t)
(use-package ivy-hydra  :ensure t)
(use-package ivy
  :ensure t
  :defer nil
  :init
  (ivy-mode 1)
  (global-set-key (kbd "M-x") 'counsel-M-x)
  (global-set-key "\C-s" 'swiper)
  (global-set-key (kbd "C-c r") 'ivy-resume)

  (setq-default ivy-use-virtual-buffers t
                ivy-virtual-abbreviate 'fullpath
                ivy-use-selectable-prompt t
                ivy-count-format ""
                projectile-completion-system 'ivy
                ivy-initial-inputs-alist)
  :config
  (setq ivy-re-builders-alist '((t . ivy--regex-ignore-order))))

(provide 'init-ivy)
