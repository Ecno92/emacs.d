(use-package go-mode  :ensure t)
(use-package company  :ensure t)
(use-package company-go  :ensure t)

(add-hook 'go-mode-hook
      (lambda ()
        (set (make-local-variable 'company-backends) '(company-go))
        (company-mode)))
(add-hook 'before-save-hook #'gofmt-before-save)

(provide 'init-golang)
