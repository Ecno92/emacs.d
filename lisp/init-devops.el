(use-package terraform-mode  :ensure t )
(use-package company  :ensure t )
(use-package company-terraform  :ensure t
  :config
  (company-terraform-init))

(defun ansible-vault-mode-maybe ()
  (when (ansible-vault--is-vault-file)
    (ansible-vault-mode 1)))

(use-package ansible-vault
  :ensure t
  :hook ((yaml-mode . 'ansible-vault-mode-maybe)))


(provide 'init-devops)
