(use-package editorconfig  :ensure t :config (editorconfig-mode 1))
(use-package nlinum :ensure t :config (global-nlinum-mode))
(use-package wgrep :ensure t)
(use-package beacon :ensure t :config (beacon-mode 1))
(setq enable-recursive-minibuffers t)
(setq-default fill-column 80)
(use-package fill-column-indicator
  :ensure t
  :config
  (setq fci-rule-column 80))
(setq-default show-trailing-whitespace t)
(put 'upcase-region 'disabled nil)
(put 'downcase-region 'disabled nil)
(put 'narrow-to-region 'disabled nil)

(global-hl-line-mode)
(use-package dimmer
  :ensure t
  :config (dimmer-mode))

(setq column-number-mode t)
(setq line-number-mode t)

(use-package avy :ensure t
  :config
  (global-set-key (kbd "C-;") 'avy-goto-char-timer)
  (global-set-key (kbd "M-g g") 'avy-goto-line))

(setq-default indent-tabs-mode nil)
(show-paren-mode t)
(add-hook 'before-save-hook 'delete-trailing-whitespace)

(use-package switch-window :ensure t
  :config
  (global-set-key (kbd "C-x o") 'switch-window)
  (setq switch-window-background t)
  (setq switch-window-threshold 4))

(winner-mode 1)

(when (require 'so-long nil :noerror)
  (so-long-enable))

(use-package windmove
  :ensure t
  :config
  (windmove-default-keybindings)
  (setq windmove-wrap-around nil))

(defalias 'yes-or-no-p 'y-or-n-p)

(provide 'init-editor)
